package com.bu.assignment.toc.setopt;

import java.util.*;

public class App {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        List<String> setA = new ArrayList<>();
        List<String> setB = new ArrayList<>();
        Integer lengthSetA = scanner.nextInt();
        for(int i = 0; i < lengthSetA; i++){
            setA.add(scanner.next());
        }
        Integer lengthSetB = scanner.nextInt();
        for(int i = 0; i < lengthSetB; i++){
            setB.add(scanner.next());
        }

        Set<String> union = new HashSet<>();
        union.addAll(setA);
        union.addAll(setB);
        TreeSet unionSorted = new TreeSet(union);
        unionSorted.stream().forEach(System.out::println);
        System.out.println();

        List<String> setATmp = new LinkedList<>(setA);
        setATmp.retainAll(setB);
        Collections.sort(setATmp);
        setATmp.stream().forEach(System.out::println);
        System.out.println();

        List<String> cartesianProduct = new LinkedList<>();
        for (String sa : setA){
            for (String sb : setB){
                cartesianProduct.add(sa+" "+sb);
            }
        }
        Collections.sort(cartesianProduct);
        cartesianProduct.stream().forEach(System.out::println);


    }
}
